<!DOCTYPE html>
<html lang="hu">
<head>
  <title>Delete Form</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" href="style.css"/>
</head>
<body>
  <?php
    require_once('constants.php');
  ?>
  <?php
  $dbc=mysqli_connect(HOST, USER, PASSWD, DB) or die('Unable to establis DB Connection');
  mysqli_query($dbc, "set names 'utf8'") or die('Unable to switch to UTF-8');
  if(isset($_GET['id'])){
        $query="DELETE FROM members WHERE id = " . $_GET['id'];
        mysqli_query($dbc, $query) or die('Unsuccessful deletion');
  }

  mysqli_close($dbc);
  ?>
  <script type="text/javascript">
    window.alert("Sucessfully edited Member in the DataBase with ID: " + <?=$_GET['id']?>)
    window.setTimeout(function() {
      window.location.href = 'deletelist.php';
    }, 250);
  </script>

  <?php
  ?>
</body>
</html>
