<!DOCTYPE html>
<html lang="hu">
<head>
  <title>Add Form</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" href="style.css"/>
</head>
<body>
  <h1>Click an ID to delete a Member from Database</h1>
  <?php
    require_once('constants.php');
    require_once('auth.php');
  ?>
  <?php
  $dbc=mysqli_connect(HOST, USER, PASSWD, DB) or die('Unable to establis DB Connection');
  mysqli_query($dbc, "set names 'utf8'") or die('Unable to switch to UTF-8');
  $query = "SELECT * FROM members ORDER BY id";
  $list=mysqli_query($dbc, $query) or die('Unsuccessful insert');
  mysqli_close($dbc);
  echo "<table>";
  ?>
  <tr>
    <th>ID</th>
    <th>Name</th>
    <th>E-mail</th>
    <th>Note</th>
    <th>Country</th>
  </tr>
  <?php
  while($line=mysqli_fetch_array($list)){
    ?>
    <tr>
      <td>
         <a href="<?='deletion.php?id=' . $line['id'] ?>">
           <?= $line['id']?>
         </a>
      </td>
      <td><?=$line['name']?></td>
      <td><?=$line['email']?></td>
      <td><?=$line['note']?></td>
      <td><?=$line['country']?></td>
    </tr>
    <?php
  }
  echo "</table>"
  ?>

  <h2><a href="index.html">&lt;&lt;Back to the Main Page</a></h2>

</body>
</html>
