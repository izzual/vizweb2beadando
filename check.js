function byId(id){
  return document.getElementById(id);
}

function byName(name) {
  return document.getElementsByName(name);
}

function isEmpty(id) {
  if(byId(id).value==''){
    return true;
  }
  return false;
}

function isAllWhiteSpace(id){
  var userInput = byId(id).value;
  userInput = userInput.replace(/^\s+/, '').replace(/\s+$/, '');
  if (userInput === ''){
    return true;
  }
  return false;
}

function isEmailValid(id) {
  var email=byId(id).value;
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
      return true;
  }
  return false;
}

function check(eo){
  var errors=[];
  if(isEmpty('email')){
    errors.push('No e-mail has been provided!');
  }
  else{
    if(!isEmailValid('email')){
      errors.push('Invalid e-mail format!');
    }
  }
  if(isEmpty('name')){
    errors.push('No name has  been provided!');
  }
  else{
    if(isAllWhiteSpace('name')){
      errors.push('Do not try to trick with whitespaces!');
    }
  }
  if(errors.length>0){
    eo.preventDefault();
    showErrors(errors);
  }
}

function setErrorListStyle(id){
  var errorlist=byId(id);
  errorlist.style.fontSize='20px';
  errorlist.style.color='red';
}


function showErrors(errors){
  setErrorListStyle('errorlist');
  var errorString='<li>'+errors.join('</li><li>')+'</li>';
  byId('errorlist').innerHTML=errorString;
}

function initialize(){
  byId('formId').addEventListener('submit', check, false)
}

window.addEventListener('load', initialize, false);
