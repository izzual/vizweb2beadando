# Vizweb2 Beadandó - Csapó Péter

## Pár kép az appról:

### Nyitóoldal - index.html

![indexhtml](https://i.imgur.com/U8NDNpk.jpg)

### Listanézet

![listphp](https://i.imgur.com/kr1Bshj.jpg)

###  Hozzáadásnézet - a csillaggal megjelölt mezők kitöltése kötelező

![addphp](https://i.imgur.com/Rf63dNr.jpg)

### Szerkesztésnézet - ID-t megadjuk, s az alapján editelhetünk

![editphp](https://i.imgur.com/amX4F2j.png)

### Törlésnézet #1 - egyszerű authentication, hogy ne törölhessen bárki

![authphp](https://i.imgur.com/ELCUBno.jpg)

### Törlésnézet #2 - ID-re kattintva tudunk törölni

![deletelistphp](https://i.imgur.com/8sSNtde.png)

### Törlésnézet #3 - Sikeres törlésről értesítést kapunk, majd "frissíti magát" a honlap

![deletionphp](https://i.imgur.com/ZpGHRvX.png)

### Adatbázisstruktúra

![dbstructure](https://i.imgur.com/sfEaDW4.jpg)


