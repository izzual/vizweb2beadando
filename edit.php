<!DOCTYPE html>
<html lang="hu">
<head>
  <title>Add Form</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" href="style.css"/>
</head>
<body>
  <h1>Edit existing Member in Database based on ID</h1>
  <?php
    require_once('constants.php');
  ?>
  <?php

$nameErr = $emailErr = "";
$name = $email = $note = $country = $id= "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  } else {
    $nameErr="";
    $name = test_input($_POST["name"]);
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and white space allowed";
    }
  }

  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } else {
    $emailErr="";
    $email = test_input($_POST["email"]);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format";
    }
  }

  if (empty($_POST["note"])) {
    $note = "";
  } else {
    $note = test_input($_POST["note"]);
  }
  if (empty($_POST["id"])) {
    $id = "";
  } else {
    $id = test_input($_POST["id"]);
  }

  if (empty($_POST["country"])) {
    $country= "";
  } else {
    $country = test_input($_POST["country"]);
  }

  if($nameErr == "" && $emailErr == "" && !$id == ""){
      $dbc=mysqli_connect(HOST, USER, PASSWD, DB) or die('Unable to establish DB Connection');
      mysqli_query($dbc, "set names 'utf8'") or die('Unable to switch to UTF-8');
      $query="UPDATE `members` SET `name`='$name',`email`='$email',`note`='$note',`country`='$country' WHERE id=$id";
      mysqli_query($dbc, $query) or die('Unsuccessful insert');
      mysqli_close($dbc);
      $name = $email = $note = $country = "";
      ?>
      <script type="text/javascript">
        window.alert("Sucessfully edited Member in the DataBase with ID: " + <?=$id?>)
      </script>

    <?php
    $id = "";
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

?>
<p align="center"><span class="error">* required field</span></p>
<form method="post" id="formId" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
  <span class="error">* <?php echo $nameErr;?></span>
  ID:<input type="text" name="id" value="<?php echo $id;?>"required>
  <br><br>
  <span class="error">* <?php echo $nameErr;?></span>
  Name: <input type="text" id="name" name="name" value="<?php echo $name;?>">
  <br><br>
  <span class="error">* <?php echo $emailErr;?></span>
  E-mail: <input type="text" id="email" name="email" value="<?php echo $email;?>">
  <br><br>
  Country: <input type="text" name="country" value="<?php echo $country;?>">
  <br><br>Note:
  <div align="center">
    <textarea placeholder="Enter member's note" name="note" rows="5" cols="40"><?php echo $note;?></textarea>
    <ul id="errorlist"></ul>
  </div>
  <br><br>
  <input type="submit" name="submit" value="Submit">
</form>
<script type="text/javascript" src="check.js"></script>
  <h2><a href="index.html">&lt;&lt;Back to the Main Page</a></h2>
</body>
</html>
